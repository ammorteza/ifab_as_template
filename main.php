<!doctype html>
<html class="no-js" lang="en" dir="rtl">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Foundation for Sites</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/font.css">
    <link rel="stylesheet" href="foundation-icons/foundation-icons.css">
    <link rel="stylesheet" href="css/key.css">
    <script src="js/Chart.js"></script>
  </head>
  <body class="BYekan">

    <!--chart1 Modal Start-->
    <div style="z-index: 9999;" class="large reveal" id="exampleModal1" data-reveal>
      <h6 class="text-center BYekan">نمودار تخصیص اعتبارات بر اساس منابع</h6>

      <canvas id="myChartM"></canvas>
      <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
      </button>
      <button type="button" class="my-secondary small button float-left" data-open="exampleModal1"><i class="fi-archive"></i> دریافت </button>
    </div>
    <!--chart1 Modal End-->
      <div style="z-index: 999;" data-sticky-container>
      <div class="top-bar top-menu-color" data-sticky data-margin-top="0">
        <div class="top-bar-left">
          <ul data-dropdown-menu class="dropdown menu" >
            <li>
              <a href="#"><span style="font-size: 14px;"> علی جهان پاک </span></a>
              <ul style="background-color: #ffffff;" class="menu vertical small-font white-color">
                <li><a href="#"><i class="fi-torso-business"></i> <span>کارشناس اداری مالی</span></a></li>
                <li><a href="#"><i class="fi-widget"></i> <span>تنظیمات</span></a></li>
                <li><a href="#"><i class="fi-power"></i> <span>خروج</span></a></li>
              </ul>
            </li>
            <li>
              <a style="margin-top: -1px;" href="#"><img style="color: #337AB7;font-size: 14px;" src="pic\bell-icon.svg" width="18px" height="18px;"></a>
              <ul style="background-color: #FFFFFF;" class="menu vertical">
                <li class="border-btm-line">
                  <a style="padding-bottom: -5px;" href="#">
                    <div>
                      <strong style="color: #000000;">علیرضا طاهری</strong>
                      <span style="color: #777;" class="message-time large-offset-7"><i class="fi-calendar size-18"></i>41 دقیقه قبل</span>
                    </div>
                    <div>
                      <p style="color:  #333;margin-top: 12px;"> موافقت نامه سال 1396 در مورد پروژه های عمرانی در دست اقدام پیگیری از سازمان</p>
                    </div>
                  </a>
                </li>



                <li class="border-btm-line">
                  <a style="padding-bottom: -5px;" href="#">
                    <div>
                      <strong style="color: #000000;">علیرضا طاهری</strong>
                      <span style="color: #777;" class="message-time large-offset-7"><i class="fi-calendar size-18"></i>دو روز پیش</span>
                    </div>
                    <div>
                      <p style="color:  #333;margin-top: 12px;"> موافقت نامه سال 1396 در مورد پروژه های عمرانی در دست اقدام پیگیری از سازمان</p>
                    </div>
                  </a>
                </li>
              </ul>
            </li>

          </ul>
        </div>
        <div class="top-bar-right">
          <button class="button dropdown small sm-btn-align"  type="button" data-toggle="example-dropdown-bottom-left">1396</button>
          <div class="dropdown-pane dropdown-pane-sm " data-close-on-click="true"  data-hover="true" data-hover-pane="true"  data-position="bottom" data-alignment="right" id="example-dropdown-bottom-left" data-dropdown data-auto-focus="true">
            <ul class="my-menu">
              <li><a  href="#">1395</a></li>
              <li><a  href="#">1394</a></li>
              <li><a  href="#">1393</a></li>
            </ul>
          </div>

        </div>
      </div>
      </div>
      <!--Body system-->
      <div class="grid-x">
        <div class="medium-2 cell small-font" data-sticky-container>
          <nav class="columns sticky" data-sticky data-anchor="exampleId" data-sticky-on="large" data-margin-top="4.3">
          <ul class="vertical menu sub-menu accordion-menu" data-accordion-menu >
            <li>
              <a class="active-menu right-menu-btm-border" href="#">توزیع اعتبار</a>
              <ul class="menu vertical nested">
                <li><a href="#">هزینه ای</a>
                  <ul class="menu vertical nested">
                    <li><a href="#">استانی</a></li>
                    <li><a href="#">ملی</a></li>
                  </ul>
                </li>
                <li><a href="#">تملک دارایی سرمایه ای</a>
                  <ul class="menu vertical nested">
                    <li><a href="#">استانی</a></li>
                    <li><a href="#">ملی</a></li>
                    <li><a class="right-menu-btm-border" href="#">پیشنهاد دستگاه ها</a></li>
                  </ul>
                </li>
              </ul>
            </li>

            <li>
              <a class="right-mp right-menu-btm-border" href="#">  موافقتنامه</a>
              <ul class="menu vertical nested">
                <li><a href="#">هزینه ای</a>
                <li><a href="#">تملک دارایی سرمایه ای</a></li>
                <li><a href="#">طرح ها</a></li>
                <li><a href="#">پروژه ها</a></li>
              </ul>
            </li>
            <li>
              <a class="right-mp right-menu-btm-border" href="#">تخصیص اعتبار</a>
              <ul class="menu vertical nested">
                <li><a href="#">هزینه ای</a>
                  <ul class="menu vertical nested">
                    <li><a href="#">بند و</a></li>
                    <li><a href="#">سایر</a></li>
                  </ul>
                </li>
                <li><a href="#">تملک دارایی سرمایه ای</a>
                  <ul class="menu vertical nested">
                    <li><a href="#">پروژه ها</a></li>
                  </ul>
                </li>
              </ul>
            </li>
          </ul>
          </nav>
        </div>

        <!--my body-->
        <?php
            include 'mm.php';
        ?>
      </div>



    <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>
